﻿using System;
namespace OOP_Git
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start employee1 Calculation");
            EmployeeBase employee1 = new EmployeeBase();
            var salaryEmployee1 = employee1.CalculateSalary();
            Console.WriteLine($"employee1 salary = {salaryEmployee1}");

            //Task #1
            // TODO: ADD implementations for ManagerEmployee
            // formula =  experience * 500 + 100 extra bonus + 900 anual
            Console.WriteLine("Start managerEmployee Calculation");
            ManagerEmployee managerEmployee = new ManagerEmployee();
            string salaryManagerEmployee = employee1.CalculateSalary();
            Console.WriteLine($"manageremployee1 salary = {salaryManagerEmployee}");

            //Task #2
            // TODO: ADD implementations for ClientExecutive
            // formula =  experience * 700 + 200 extra bonus + 700 ensurance + 900 anual
            Console.WriteLine("Start clientExecutiveTest Calculation");
            ClientExecutive clientExecutiveTest = new ClientExecutive();
            string salaryClientExecutiveTest = clientExecutiveTest.CalculateSalary();
            Console.WriteLine($"ClientExecutiveTest Salary = {salaryClientExecutiveTest}");

            //Task #3
            // TODO: ADD implementations for IT
            // formula =  experience * 200 + 100 + 900 anual
            Console.WriteLine("Start IT guy Calculation");
            IT ITguy = new IT();
            string salaryITguy = ITguy.CalculateSalary();
            Console.WriteLine($"ITguy salary = {salaryITguy}");
            
            //Task #4
            // TODO: ADD implementations for SeniorDeveloper 
            // formula =  experience * 800 + 100 + extra bonus 700 Euro anual           
            Console.WriteLine("Start the Senior Developer Calculation");
            SeniorDeveloper seniorDev = new SeniorDeveloper();
            string salarySeniorDev = seniorDev.CalculateSalary();
            Console.WriteLine($"Senior Developer salary = {salarySeniorDev}");
            
            //Task #5
            // TODO: ADD implementations for MeedDeveloper
            // formula =  experience * 500 + 100 extra bonus + 900 anual

            Console.WriteLine("Start meedDev Calculation");
            MeedDeveloperBase meedDev = new MeedDeveloperBase();
            var salaryMeedDev = meedDev.CalculateSalary();
            Console.WriteLine($"meedDev salary = {salaryMeedDev}");

            //Task #6
            // TODO: ADD implementations for JuniorDeveloper
            Console.WriteLine("Start JuniorDev Calculation");
            JuniorDeveloperBase JuniorDev = new JuniorDeveloperBase();
            var salaryJuniorDev = JuniorDev.CalculateSalary();
            Console.WriteLine($"JuniorDev salary = {salaryJuniorDev}");


            //Task #7
            // TODO: ADD implementations for ArchitectDEV
            // formula =  experience * 200 + 100 + 900 anual
            Console.WriteLine("Starting architect test");
            IEmployee architectDev = new ArchitectDEV();
           string  architectDevSalary = architectDev.CalculateSalary();
            Console.WriteLine("An architect   developer's salary : "+architectDevSalary);

            //Task #8
            // TODO: ADD implementations for QA
            // formula =  experience * 800 + 100 + extra bonus 700 Euro anual

            // Task #9
            // TODO: ADD implementations for KfcWorker
            // formula = experience * 800 + 500
            Console.WriteLine("Start the KFC Worker Calculation");
            KfcWorker kfcWorker = new KfcWorker();
            string salaryKfcWorker = kfcWorker.CalculateSalary();
            Console.WriteLine($"Kfc Worker salary = {salaryKfcWorker}");

            //Task #10
            // TODO: ADD implementations for HR
            // formula =  experience * 250 + 100 bonus + 1200 anual
            Console.WriteLine("Start HR Calculation");
            HR employeeHR = new HR();
            var salaryHR = employeeHR.CalculateSalary();
            Console.WriteLine($"HR salary = {salaryHR}");

            Console.WriteLine("Start QAengineer calculation");
            IEmployee QAengineer = new QA();
            string  qaSalary = QAengineer.CalculateSalary();
            Console.WriteLine("QA salary : "+qaSalary);

            //Task #11 
            //TODO: ADD implementations for  Witcher
            Console.WriteLine("Start Witcher calculation.Starting 'toss a coin to your witcher' tests.");
            IEmployee witcher = new Witcher();
            string witcherSalary = witcher.CalculateSalary();
            Console.WriteLine("Witcher salary :" + witcherSalary);
         

            //Task George 1v2
            // TODO: ADD implementations for SupremeDirectorExecutiveManager
            // formula =  experience * a lot salary
            SupremeDirectorExecutiveManager supremeDirectorExecutiveManager = new SupremeDirectorExecutiveManager();
            Console.WriteLine($"supremeDirectorExecutiveManager salary: {supremeDirectorExecutiveManager.CalculateSalary()}");
            Console.WriteLine("Start janitor calculation");
            Janitor Michael = new Janitor();
            string janitorSalary = Michael.CalculateSalary();
            Console.WriteLine("Janitor salary : " + janitorSalary);


            //Task #13
            // TODO: ADD implementation for McDonaldsWorker
            // formula = experience * 200 + 300 anual 
            Console.WriteLine("Start McDonaldsWorker Calculation");
            McDonaldsWorker mcWorker = new McDonaldsWorker();
            string salaryMcWorker = mcWorker.CalculateSalary();
            Console.WriteLine($"McDonalds Worker salary = {salaryMcWorker}");

            //Task RIP portart ciocio
            // TODO: ADD implementation for Portar
            // formula = experience * 9000 + 1
            Console.WriteLine("Start GoalKeeper Calculation");
            GoalKeeper goalKeeper = new GoalKeeper();
            string salaryGoalKeeper = goalKeeper.CalculateSalary();
            Console.WriteLine($"GoalKeeper salary = {salaryGoalKeeper}");

            //Task #14
            //TODO: ADD implementation for Dragonborn 
            Console.WriteLine("Start  dragonborn calculations");

            IEmployee dragonborn = new Dragonborn();
            string  dragonbornSalary = dragonborn.CalculateSalary();
            Console.WriteLine("Dragonborn worker salary : " + dragonbornSalary);

        }
    }
}
