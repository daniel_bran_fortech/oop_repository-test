﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_Git
{
    class ManagerEmployee : IEmployee
    {
        private int experience = 10;
        public const int netAmount = 500;
        public const int extraBonus = 100;
        public const int annual = 900;
        public string CalculateSalary()
        {
            return (experience * netAmount + extraBonus + annual).ToString();
        }
    }
}
