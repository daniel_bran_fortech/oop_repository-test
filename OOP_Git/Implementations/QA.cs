﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_Git
{
    class QA : IEmployee
    {
        int _experience = 5;
        int _presence = 800;
        int _projectBonus = 100;
        int _anualBonus = 700;
        public string CalculateSalary()
        {
            return (_experience * _presence +_projectBonus + _anualBonus).ToString();
        }
    }
}
