﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_Git
{
    class ClientExecutive : IEmployee
    {
        public int experience = 5;
        public const int netAmount = 700;
        public const int extraBonus = 200;
        public const int ensurance = 700;
        public const int annual = 900;
        public string CalculateSalary()
        {
            return (experience * netAmount + extraBonus + ensurance + annual).ToString();
        }
    }
}
