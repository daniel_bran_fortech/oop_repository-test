﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_Git
{
    class SeniorDeveloper : IEmployee
    {
        // formula =  experience * 800 + 100 + extra bonus 700 Euro anual
        private int _experience = 5;
        int extraBonusAnual = 700;
        int weeklyBonus = 800;
        int extraBonus = 100;
        public string CalculateSalary()
        {
            return (weeklyBonus * _experience + extraBonus + extraBonusAnual).ToString();
        }
    }
}
