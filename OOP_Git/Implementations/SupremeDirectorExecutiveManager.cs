﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_Git
{
    class SupremeDirectorExecutiveManager : IEmployee
    {
        public int experience = 50;
        public int netAmount = 100000;
        public string CalculateSalary()
        {
            return (experience * netAmount).ToString();
        }
    }
}
