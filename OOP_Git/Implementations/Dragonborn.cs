﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP_Git
{
    class Dragonborn : IEmployee
    {
        private int dragonsSlayed = 56;
        private int childrenBonus = 500;
        private int marriedBonus = 250;
        private  int  mainStoryCompleted = 250;
        public string CalculateSalary()
        {
            int salary = dragonsSlayed * 3 + childrenBonus * 4 +
                          marriedBonus * 2 + mainStoryCompleted * 2;
            return salary.ToString();

        }
    }
}
