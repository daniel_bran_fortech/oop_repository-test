﻿namespace OOP_Git
{
    public class EmployeeBase : IEmployee
    {
        private int _experience = 5;
        public string CalculateSalary()
        {
            return (100 * _experience).ToString();
        }
    }
}
