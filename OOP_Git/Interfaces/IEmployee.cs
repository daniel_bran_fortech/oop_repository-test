﻿namespace OOP_Git
{
    // IEmployee
    interface IEmployee
    {
        string CalculateSalary();
    }
}
